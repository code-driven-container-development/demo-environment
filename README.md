# demo-environment

Install OpenShift GitOps operator and run the following commands to create the environment:

```sh
# Grand admin permisions to ArgoCD user: 
oc adm policy add-cluster-role-to-user cluster-admin  system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n openshift-gitops

# Create demo application
oc apply -f code-driven-container-development.yaml
```
